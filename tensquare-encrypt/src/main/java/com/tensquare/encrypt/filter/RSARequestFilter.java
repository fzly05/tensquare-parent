package com.tensquare.encrypt.filter;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.netflix.zuul.http.ServletInputStreamWrapper;
import com.tensquare.encrypt.rsa.RsaKeys;
import com.tensquare.encrypt.service.RsaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RSARequestFilter extends ZuulFilter {

    @Autowired
    private RsaService rsaService;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //过滤器执行的具体逻辑
        System.out.println("过滤器执行!");

        //获取requestcontext容器
        RequestContext context = RequestContext.getCurrentContext();
        //铜鼓哦容器后去req resp
        HttpServletRequest request = context.getRequest();
        HttpServletResponse response = context.getResponse();
        //声明存放加密后的数据变量
        String requestData = null;
        //声明解密后的数据变量
        String decryptData = null;
        //通过request获取inputStream
        try {
            ServletInputStream inputStream = request.getInputStream();
            //从流中得到加密后的数据
            requestData = StreamUtils.copyToString(inputStream, Charsets.UTF_8);
            System.out.println("requestData = " + requestData);
            //解密操作
            if (!Strings.isNullOrEmpty(requestData)) {
                decryptData=rsaService.RSADecryptDataPEM(requestData, RsaKeys.getServerPrvKeyPkcs8());
                System.out.println("decryptData = " + decryptData);
            }
            //解密后的数据进行转发,需要放大request中
            if (!Strings.isNullOrEmpty(requestData)) {
                //获取解密后的字节数据
                byte[] dataBytes = decryptData.getBytes();
                //使用requestContext进行转发
                context.setRequest(new HttpServletRequestWrapper(request){
                    @Override
                    public int getContentLength() {
                        return dataBytes.length;
                    }
                    @Override
                    public long getContentLengthLong() {
                        return dataBytes.length;
                    }
                    @Override
                    public ServletInputStream getInputStream() throws IOException {
                        return new ServletInputStreamWrapper(dataBytes);
                    }
                });
            }
            //设置请求头
            context.addZuulRequestHeader("Context-Type", MediaType.APPLICATION_JSON_UTF8_VALUE );
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }
}
