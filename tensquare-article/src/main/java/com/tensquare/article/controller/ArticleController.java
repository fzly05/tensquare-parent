package com.tensquare.article.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tensquare.article.pojo.Article;
import com.tensquare.article.service.ArticleService;
import com.tensquare.entity.PageResult;
import com.tensquare.entity.Result;
import com.tensquare.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("article")
@CrossOrigin
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("hello")
    public String hello() {
        return "Hello SpringBoot!";
    }


    /**
     * 文章订阅
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "subscribe", method = RequestMethod.POST)
    public Result subscribe(@RequestBody Map map) {
        Boolean flag = articleService.subscribe(map.get("articleId").toString(), map.get("userId").toString());
        if (flag) {
            return new Result(true, StatusCode.OK, "订阅成功!");
        } else {
            return new Result(true, StatusCode.OK, "取消订阅!");
        }
    }

    /**
     * 查询全部文章
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        List<Article> articleList = articleService.findAll();
        return new Result(true, StatusCode.OK, "查询所有文章成功!", articleList);
    }

    /**
     * 根据id查询文章
     *
     * @param articleId 文章id
     * @return
     */
    @RequestMapping(value = "{articleId}", method = RequestMethod.GET)
    public Result findById(@PathVariable String articleId) {
        Article article = articleService.findById(articleId);
        return new Result(true, StatusCode.OK, "查询文章成功(根据id)!", article);
    }

    /**
     * 添加文章
     *
     * @param article
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result save(@RequestBody Article article) {
        articleService.save(article);
        return new Result(true, StatusCode.OK, "添加文章成功!");
    }

    /**
     * 修改文章
     *
     * @param articleId
     * @param article
     * @return
     */
    @RequestMapping(value = "{articleId}", method = RequestMethod.PUT)
    public Result update(@PathVariable String articleId, @RequestBody Article article) {
        article.setId(articleId);
        articleService.updateById(article);
        return new Result(true, StatusCode.OK, "修改文章成功!");
    }

    /**
     * 删除文章
     *
     * @param articleId
     * @return
     */
    @RequestMapping(value = "{articleId}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String articleId) {
        articleService.deleteById(articleId);
        return new Result(true, StatusCode.OK, "删除文章成功!");
    }

    @RequestMapping(value = "search/{page}/{size}", method = RequestMethod.POST)
    public Result findByPage(@PathVariable Integer page, @PathVariable Integer size, @RequestBody Map<String, Object> map) {
        //根据条件分页查询
        Page<Article> pageData = articleService.findByPage(map, page, size);
        //封装分页数据返回
        PageResult<Article> articlePageResult = new PageResult<>();
        articlePageResult.setTotal(pageData.getTotal());
        articlePageResult.setRows(pageData.getRecords());
        return new Result(true, StatusCode.OK, "分页查询文章成功!", articlePageResult);
    }

    //根据文章id点赞文章
    //文章点赞
    @RequestMapping(value = "thumbup/{articleId}", method = RequestMethod.PUT)
    public Result thumbup(@PathVariable String articleId) {
        //模拟用户id
        String userId = "4";
        String key = "thumbup_article_" + userId + "_" + articleId;
        //查询用户点赞信息，根据用户id和文章id
        Object flag = redisTemplate.opsForValue().get(key);
        //判断查询到的结果是否为空
        if (flag == null) {
            //如果为空，表示用户没有点过赞，可以点赞
            articleService.thumbup(articleId,userId);
            //点赞成功，保存点赞信息
            redisTemplate.opsForValue().set(key, 1);
            return new Result(true, StatusCode.OK, "点赞成功");
        }
        //如果不为空，表示用户点过赞，不可以重复点赞
        return new Result(false, StatusCode.REPERROR, "不能重复点赞");
    }

}
