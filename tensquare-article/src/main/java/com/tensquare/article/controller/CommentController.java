package com.tensquare.article.controller;

import com.tensquare.article.pojo.Comment;
import com.tensquare.article.service.CommentServcie;
import com.tensquare.entity.Result;
import com.tensquare.entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comment")
public class CommentController {

    @Autowired
    private CommentServcie commentServcie;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 查询所有评论
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        List<Comment> commentList = commentServcie.findAll();
        return new Result(true, StatusCode.OK, "查询所有评论成功!", commentList);
    }

    /**
     * 根据id查询评论
     *
     * @param commentId
     * @return
     */
    @RequestMapping(value = "{commentId}", method = RequestMethod.GET)
    public Result findById(@PathVariable String commentId) {
        Comment comment = commentServcie.findById(commentId);
        return new Result(true, StatusCode.OK, "查询评论成功(id)!", comment);
    }

    /**
     * 新增评论
     *
     * @param comment
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result save(@RequestBody Comment comment) {
        commentServcie.save(comment);
        return new Result(true, StatusCode.OK, "新增评论成功!", comment);
    }

    /**
     * 修改评论
     *
     * @param commentId
     * @param comment
     * @return
     */
    @RequestMapping(value = "{commentId}", method = RequestMethod.PUT)
    public Result updateById(@PathVariable String commentId, @RequestBody Comment comment) {
        //设置评论主键
        comment.setId(commentId);
        //执行修改
        commentServcie.updateById(comment);
        return new Result(true, StatusCode.OK, "修改评论成功!");
    }

    /**
     * 删除评论
     *
     * @param commentId
     * @return
     */
    @RequestMapping(value = "{commentId}", method = RequestMethod.DELETE)
    public Result deleteById(@PathVariable String commentId) {
        commentServcie.deleteById(commentId);
        return new Result(true, StatusCode.OK, "删除评论成功!");
    }

    /**
     * 根据文章id查询评论
     *
     * @param articleId
     * @return
     */
    @RequestMapping(value = "article/{articleId}", method = RequestMethod.GET)
    public Result findByArticleId(@PathVariable String articleId) {
        List<Comment> commentList = commentServcie.findByArticleId(articleId);
        return new Result(true, StatusCode.OK, "根据文章id查询评论成功!", commentList);
    }

    /**
     * 点赞功能
     * @param commentId
     * @return
     */
    @RequestMapping(value = "thumbup/{commentId}", method = RequestMethod.PUT)
    public Result thumbup(@PathVariable String commentId) {
        //用户的点赞信息保存道redis中
        String userId = "864956126";
        //在redis中查询用户是否已经点赞
        Object flag = redisTemplate.opsForValue().get("thumbup_" + userId + "_" + commentId);
        //如果点赞不能重复点赞
        if (flag == null) {
            //如果没有点赞，可以进行点赞操作
            commentServcie.thumbup(commentId);
            //保存点赞记录
            redisTemplate.opsForValue().set("thumbup_" + userId + "_" + commentId, 1);
            return new Result(true, StatusCode.OK, "点赞成功!");
        }
        //已经点过赞,不能点赞
        return new Result(false, StatusCode.ERROR, "点赞失败,不能重复点赞!!");



    }


}
