package com.tensquare.article.service;

import com.tensquare.article.pojo.Comment;
import com.tensquare.article.repository.CommentRepository;
import com.tensquare.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentServcie {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Comment> findAll() {
        List<Comment> commentList = commentRepository.findAll();
        return commentList;
    }

    public Comment findById(String commentId) {
        Comment comment = commentRepository.findById(commentId).get();
        return comment;
    }

    public void save(Comment comment) {
        String id = idWorker.nextId() + "";
        comment.setId(id);
        comment.setThumbup(0);
        comment.setPublishdate(new Date());
        commentRepository.save(comment);
    }

    public void updateById(Comment comment) {
        commentRepository.save(comment);
    }

    public void deleteById(String commentId) {
        commentRepository.deleteById(commentId);
    }

    public List<Comment> findByArticleId(String articleId) {
        List<Comment> commentList = commentRepository.findByArticleid(articleId);
        return commentList;
    }

    public void thumbup(String commentId) {
        //普通模式
        /*Comment comment = commentRepository.findById(commentId).get();
        comment.setThumbup(comment.getThumbup()+1);
        commentRepository.save(comment);*/

        //点赞优化
        //直接修改数据,
        //修改条件
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(commentId));
        //修改数值
        Update update = new Update();
        //使用inc增长实现
        update.inc("thumbup", 1);
        //集合名
        mongoTemplate.updateFirst(query, update, "comment");

    }
}
