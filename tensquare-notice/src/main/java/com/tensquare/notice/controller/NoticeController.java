package com.tensquare.notice.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.tensquare.entity.PageResult;
import com.tensquare.entity.Result;
import com.tensquare.entity.StatusCode;
import com.tensquare.notice.pojo.Notice;
import com.tensquare.notice.pojo.NoticeFresh;
import com.tensquare.notice.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("notice")
@CrossOrigin
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    /**
     * 根据id查询消息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        Notice notice = noticeService.findById(id);
        return new Result(true, StatusCode.OK, "根据id查询消息成功!", notice);
    }

    /**
     * 根据条件分页查询消息
     *
     * @param page
     * @param size
     * @param notice
     * @return
     */
    @RequestMapping(value = "search/{page}/{size}", method = RequestMethod.GET)
    public Result findByPage(@PathVariable Integer page, @PathVariable Integer size, @RequestBody Notice notice) {
        Page<Notice> pageData = noticeService.findByPage(notice, page, size);
        PageResult<Notice> pageResult = new PageResult<>(pageData.getTotal(), pageData.getRecords());
        return new Result(true, StatusCode.OK, "分页查询消息成功!", pageResult);
    }

    /**
     * 新增消息
     *
     * @param notice
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result save(@RequestBody Notice notice) {
        noticeService.save(notice);
        return new Result(true, StatusCode.OK, "新增消息成功!");
    }

    /**
     * 修改消息
     *
     * @param notice
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public Result updateById(@RequestBody Notice notice) {
        noticeService.updateById(notice);
        return new Result(true, StatusCode.OK, "修改消息成功!");
    }


    /**
     * 根据用户id分页查询待推送消息
     *
     * @param page
     * @param size
     * @param userId
     * @return
     */
    @RequestMapping(value = "fresh/{userId}/{page}/{size}", method = RequestMethod.GET)
    public Result freshPage(@PathVariable Integer page, @PathVariable Integer size, @PathVariable String userId) {
        Page<NoticeFresh> pageData = noticeService.freshPage(userId, page, size);
        PageResult<NoticeFresh> pageResult = new PageResult<>(pageData.getTotal(), pageData.getRecords());
        return new Result(true, StatusCode.OK, "分页查询待推送消息成功!", pageResult);
    }

    /**
     * 删除待推送该消息
     *
     * @param noticeFresh
     * @return
     */
    @RequestMapping(value = "fresh", method = RequestMethod.DELETE)
    public Result freshDelete(@RequestBody NoticeFresh noticeFresh) {
        noticeService.freshDelete(noticeFresh);
        return new Result(true, StatusCode.OK, "删除待推送消息成功!");
    }



}
