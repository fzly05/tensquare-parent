package com.tensquare.notice.pojo;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@TableName("tb_notice_fresh")
@Data
public class NoticeFresh {
    private String userId;
    private String noticeId;
}
