package com.tensquare.notice.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.tensquare.notice.client.ArticleClient;
import com.tensquare.notice.client.UserClient;
import com.tensquare.notice.dao.NoticeDao;
import com.tensquare.notice.dao.NoticeFreshDao;
import com.tensquare.notice.pojo.Notice;
import com.tensquare.notice.pojo.NoticeFresh;
import com.tensquare.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class NoticeService {

    @Autowired
    private NoticeDao noticeDao;

    @Autowired
    private NoticeFreshDao noticeFreshDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private ArticleClient articleClient;

    @Autowired
    private UserClient userClient;

    private void getInfo(Notice notice) {
        //查询用户昵称
        HashMap userMap= (HashMap) userClient.selectById(notice.getOperatorId()).getData();
        //设置操作者用户昵称
        notice.setOperatorName(userMap.get("nickname").toString());
        //查询对象名称
        HashMap articleMap= (HashMap) articleClient.findById(notice.getTargetId()).getData();
        //设置对象名称到消息中去
        notice.setTargetName(articleMap.get("title").toString());
    }

    public Notice findById(String id) {
        Notice notice = noticeDao.selectById(id);
        getInfo(notice);
        return notice;
    }

    public Page<Notice> findByPage(Notice notice, Integer page, Integer size) {
        //封装分页对象
        Page<Notice> pageData = new Page<>(page,size);
        //执行分页查询
        List<Notice> noticeList = noticeDao.selectPage(pageData, new EntityWrapper<>(notice));
        for (Notice n : noticeList) {
            getInfo(n);
        }
        //设置结果到对象中
        pageData.setRecords(noticeList);
        return pageData;
    }

    public void save(Notice notice) {
        //设置初始状态  status  未读0 已读1
        notice.setState("0");
        notice.setCreatetime(new Date());

        String id = idWorker.nextId() + "";
        notice.setId(id);
        noticeDao.insert(notice);

        //新消息待推送的提醒
/*        NoticeFresh noticeFresh = new NoticeFresh();
        noticeFresh.setNoticeId(id);
        noticeFresh.setUserId(notice.getReceiverId());//待通知用户的id
        noticeFreshDao.insert(noticeFresh);*/

    }

    public void updateById(Notice notice) {
        noticeDao.updateById(notice);
    }

    public Page<NoticeFresh> freshPage(String userId, Integer page, Integer size) {
        NoticeFresh noticeFresh = new NoticeFresh();
        noticeFresh.setUserId(userId);

        Page<NoticeFresh> pageData = new Page<>(page,size);

        List<NoticeFresh> noticeFreshList = noticeFreshDao.selectPage(pageData, new EntityWrapper<>(noticeFresh));

        pageData.setRecords(noticeFreshList);

        return pageData;
    }

    public void freshDelete(NoticeFresh noticeFresh) {
        noticeFreshDao.delete(new EntityWrapper<>(noticeFresh));
    }
}
